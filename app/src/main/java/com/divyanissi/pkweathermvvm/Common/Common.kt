package com.divyanissi.pkweathermvvm.Common

import java.sql.Date
import java.sql.Timestamp
import java.text.SimpleDateFormat

object  Common {
    val API_KEY = "8a43351683bf224252f27f11953827ef"
    val API_LINK = "http://api.openweathermap.org/data/2.5/weather"
    val CITY: String = "Hyderabad"

    fun apiRequest(lat:String, lng:String):String {
        val sb = StringBuilder(API_LINK)
        sb.append("?q=${CITY}&units=metric&appid=${API_KEY}")
        return sb.toString()
    }

    fun unixTimeStampToDateTime(unixTimestamp:Double):String {
        val dateformat = SimpleDateFormat("HH:mm")
        val date = java.util.Date()
        date.time = unixTimestamp.toLong()*1000
        return  dateformat.format(date)
    }

    fun getImage(icon:String):String {
        return  "http://openweathermap.org/img/w/$(icon).png"
    }

    val dateNow: String
        get() {
            val dateFormat = SimpleDateFormat("dd MM yyyy HH:mm")
            val date = java.util.Date()
            return dateFormat.format(date)
        }
}